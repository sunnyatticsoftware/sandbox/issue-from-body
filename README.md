# issue-from-body

Repository created to serve issue https://github.com/aws/aws-lambda-dotnet/issues/819

This application demonstrates how to successfully a middleware can tweak the request feature to redirect any http request to a specific endpoint as a POST with a body payload that gets properly deserialized with the `[FromBody]`
