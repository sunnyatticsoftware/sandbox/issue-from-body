﻿using Microsoft.AspNetCore.Mvc;

namespace Issue.FromBody.Controllers
{
    [Route("custom")]
    public class CustomController
        : ControllerBase
    {
        [HttpPost]
        public IActionResult AddSample([FromBody] Sample sample)
        {
            return Ok(sample);
        }
    }
}