﻿using Microsoft.AspNetCore.Mvc;

namespace Issue.FromBody.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UnreachableController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get()
        {
            return Ok("You'll never get here because the middleware will route requests to this endpoint towards POST custom/");
        }
    }
}
