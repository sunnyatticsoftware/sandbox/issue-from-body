using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

namespace Issue.FromBody
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Issue.FromBody", Version = "v1" });
            });
        }

        private string Serialize(object obj)
        {
            var options =
                new JsonSerializerOptions(JsonSerializerDefaults.Web)
                {
                    WriteIndented = true
                };
            var result = JsonSerializer.Serialize(obj, options);
            return result;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Issue.FromBody v1"));
            }

            app.Use(async (context, next) =>
            {
                // any request to /unreachable path gets tweaked into a POST /custom with sample body
                if (context.Request.Path.ToString().Contains("unreachable", StringComparison.InvariantCultureIgnoreCase))
                {
                    var requestFeature = context.Features.Get<IHttpRequestFeature>();
                    var sample = new Sample {Message = "this is my new message"};
                    var bodyJson = Serialize(sample);
                    var bodyJsonBytes = Encoding.UTF8.GetBytes(bodyJson);
                    var memoryStream = new MemoryStream(bodyJsonBytes);
                    requestFeature.Path = $"/custom";
                    requestFeature.Method = "POST";
                    requestFeature.Headers.Add("Content-Type", "application/json");
                    requestFeature.Headers.Add("Accepts", "application/json");
                    requestFeature.Body = memoryStream;
                }
                
                await next.Invoke();
            });
            
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
